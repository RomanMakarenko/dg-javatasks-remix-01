import java.util.Random;

/*Задание #1
Заданы 3 числа. Необходимо определить является ли третье число  одновременно больше результата сложения и умножения первых двух.
Входные данные
3 числа, либо как отдельные переменные, либо как элементы массива.
Выходные данные
Необходимо вывести на консоль true, если третье число одновременно больше суммы и произведения, иначе – false.
Примеры
1	4 6 11	false
2	4 6 30	true
*/

/**
 * Created by rmakaren on 20.10.2017.
 */
public class problem1 {

        public static void main(String[] args) {

            int nums [] = {1,2,3,4,5};

            if (nums[0] + nums[1] < nums[2] & nums[0] * nums[1] < nums[2]) {
                System.out.println("true");
            }

            else System.out.println("false");

            }
        }



