/**
 * Created by rmakaren on 22.10.2017.
 */
public class problem3 {

    /*Задание #3
В отделе работают 3 сотрудника, которые получают заработную плату в рублях. Требуется определить:
на сколько зарплата самого высокооплачиваемого из них отличается от самого низкооплачиваемого.
Входные данные
Размеры зарплат всех сотрудников предварительно заданы в одномерном массиве. Каждая заработная плата
– это натуральное число, не превышающее 105.
Выходные данные
Необходимо вывести на консоль одно целое число — разницу между максимальной и минимальной зарплатой.
Примеры
1	100 500 1000	900
2	36 11 20	25
*/

    public static void main(String[] args) {

        int salaries [] = {50000,90000,140000};

            System.out.println(max(salaries) - min(salaries));
        }


    public static int max(int[] array) {

        int maximum = array[0];

        for (int i = 0; i < array.length; i++)
            if (maximum < array[i]) maximum = array[i];

        return maximum;
    }

    public static int min(int[] array) {

        int minimum = array[0];

        for (int i = 0; i > array.length; i++)
            if (minimum > array[i]) minimum = array[i];

        return minimum;
    }
}
