/**Задание #2
 Известны результаты каждой из 4х четвертей баскетбольной встречи. Нужно определить победителя матча.
 Входные данные
 Результаты 4x четвертей заданы как двумерный массив 2x4, где каждый элемент (0 ≤ a,b ≤ 100). Данный массив предварительно заполнен.
 Выходные данные
 На консоль необходимо вывести номер команды выигравшей матч или DRAW, если была ничья.
 Примеры
 №	INPUT.TXT	OUTPUT.TXT
 1	26 17
 13 15
 19 11
 14 16	1
 2	14 15
 17 18
 20 20
 15 17	2
 3	15 16
 18 17
 10 12
 14 12	DRAW
 /

/**
 * Created by rmakaren on 20.10.2017.
 */
public class problem2 {

        public static void main(String[] args) {

            int basket [][] = new int [4][2] ;

            basket[0][0] = 34;
            basket[0][1] = 56;
            basket[1][0] = 13;
            basket[1][1] = 43;
            basket[2][0] = 89;
            basket[2][1] = 99;
            basket[3][0] = 1;
            basket[3][1] = 7;

            int i = 0;
            int j = 0;
            int firstTeamPoints = 0;
            int secondTeamPoints = 0;

            for (i = 0; i<4; i++) {

                firstTeamPoints = firstTeamPoints + basket[i][0];
                System.out.println(firstTeamPoints);
            }

            for (i = 0; i<4; i++) {

                secondTeamPoints = secondTeamPoints + basket[i][1];
                System.out.println(secondTeamPoints);
            }

            if (firstTeamPoints>secondTeamPoints) {

                System.out.println("firstTeam won");

            } else System.out.println("secondTeam won");


        }
    }
